import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class Logic {

    //2
    long getDayNumber(List<MonitoredData> activities) {
        int count = 1;
        Calendar cal = Calendar.getInstance();
        count = (int) activities
                .stream()
                .map(MonitoredData::getStartTime)
                .map(s -> {
                    cal.setTimeInMillis(s.getTime());
                    return cal.get(Calendar.DAY_OF_MONTH);
                })
                .distinct()
                .count();

        return count;
    }

    //3
    Map<String, Integer> getActivityCount(List<MonitoredData> activities) throws FileNotFoundException {
        final PrintWriter pw = new PrintWriter("activityCount.txt");
        Map<String, Integer> map = new HashMap<>();
        activities
                .stream()
                .map(s -> {
                    if (!map.keySet().contains(s.getActivityName()))
                        map.put(s.getActivityName(), 0);
                    map.computeIfPresent(s.getActivityName(), (k, v) -> v + 1);
                    return s;
                })

                .map(s -> String.format("%-20s %s", s.getActivityName(), map.get(s.getActivityName())))
                .forEach(String::toCharArray);

        map
                .forEach((k, v) -> {
                    pw.printf("%-20s %02d", k, v);
                    pw.println();
                    pw.flush();
                });
        return map;
    }

    //4
    void getActivityPerDay(List<MonitoredData> activities) throws FileNotFoundException {
        Map<String, List<Integer>> map = new HashMap<>();
        Calendar cal = (Calendar.getInstance());


        activities
                .stream()
                .peek(s -> {
                    List<Integer> list = new ArrayList<>();
                    cal.setTimeInMillis(s.getStartTime().getTime());
                    list.add(cal.get(Calendar.DAY_OF_MONTH));
                    if (map.keySet().contains(s.getActivityName())) {
                        map.get(s.getActivityName()).add(cal.get(Calendar.DAY_OF_MONTH));
                    } else {
                        map.put(s.getActivityName(), list);
                    }
                })
                .forEach(MonitoredData::getActivityName);

        final PrintWriter pw = new PrintWriter("activiyperday.txt");

        map.forEach((k, v) -> {
            List<Integer> list = v;
            Map<Integer, Integer> aux = new HashMap<>();
            for (Integer i : list) {
                if (aux.keySet().contains(i)) {
                    aux.put(i, aux.get(i) + 1);
                } else
                    aux.put(i, 1);
            }

            for (int i = 28; i <= 41; i++) {
                if (!aux.keySet().contains(i % 30)) {
                    if (i % 30 != 0)
                        aux.put(i % 30, 0);
                }
            }

            pw.print(String.format("%-20s %s", k, aux));
            pw.println();
            pw.flush();
            v.addAll(Collections.singleton(aux.get(k)));

        });
    }

    //5
    void duration(List<MonitoredData> activities) throws FileNotFoundException {
        final PrintWriter pw = new PrintWriter("duration.txt");
        //System.out.println("\n5. Duration for each activity: \n");
        activities
                .stream()
                .map(s -> {
                    long aux = (s.getEndTime().getTime() - s.getStartTime().getTime());
                    return aux + " " + s.getActivityName();
                })
                .map(s -> {
                    String[] aux = s.split(" ");
                    s = aux[0];
                    String name = aux[1];
                    long time = Long.parseLong(s);
                    long sec = time / 1000 % 60;
                    long min = time / (60 * 1000) % 60;
                    long h = time / (60 * 60 * 1000) % 24;
                    long day = time / (24 * 60 * 60 * 1000);
                    return (String.format("%-20s %02d:%02d:%02d:%02d", name, day, h, min, sec));
                })
                .map(s -> {
                    pw.printf("%s", s);
                    pw.println();
                    pw.flush();
                    return s;
                })
                .forEach(x -> x.toCharArray());
    }

    //6
    void entireDuration(List<MonitoredData> activities) {
        Map<String, Long> duration = new LinkedHashMap<>();
        activities
                .stream()
                .map(s -> {
                    long time = s.getEndTime().getTime() - s.getStartTime().getTime();
                    if (duration.keySet().contains(s.getActivityName())) {
                        duration.put(s.getActivityName(), duration.get(s.getActivityName()) + time);
                    } else
                        duration.put(s.getActivityName(), time);
                    return s;
                })
                .forEach(Object::toString);
        Map<String, String> totalDuration = new LinkedHashMap<>();
        duration.forEach(
                (k, v) -> {
                    long time = v;
                    long sec = time / 1000 % 60;
                    long min = time / (60 * 1000) % 60;
                    long h = time / (60 * 60 * 1000) % 24;
                    long day = time / (24 * 60 * 60 * 1000);

                    String string = String.format("%02d:%02d:%02d:%02d", day, h, min, sec);
                    totalDuration.put(k, string);
                });

        writeActivity(totalDuration);
    }

    //7
    void filter(List<MonitoredData> activities) throws FileNotFoundException {
        Map<String, Integer> count = getActivityCount(activities);
        Map<String, Integer> duration = new LinkedHashMap<>();
        List<String> filtered = new ArrayList<>();

        activities
                .stream()
                .map(s -> {
                    if (!duration.keySet().contains(s.getActivityName())) {
                        duration.put(s.getActivityName(), 0);
                    }
                    long time = s.getEndTime().getTime() - s.getStartTime().getTime();
                    if (time <= 300000) {
                        duration.put(s.getActivityName(), duration.get(s.getActivityName()) + 1);
                    }

                    return s;
                })
                .forEach(MonitoredData::getStartTime);

        duration.forEach((k, v) -> {
            if (v > 90.0f * count.get(k) / 100.0f) {
                filtered.add(k + " " + v);
            }
        });

        System.out.println("7. " + filtered.toString());

    }

    private void writeActivity(Map<? extends String, ?> map) {

        String[] headers = new String[map.keySet().size()];
        int idx = -1;
        for (String s : map.keySet()) {
            headers[++idx] = s;
        }

        try {
            PrintWriter pw = new PrintWriter("entireDuration.txt");
            pw.print("Activity list with total duration for each activity");
            pw.println();
            pw.println();
            pw.flush();
            for (int i = 0; i < headers.length; i++) {
                pw.printf("%-30s %20s", headers[i], map.get(headers[i].toString()));
                pw.println();
                pw.flush();
            }

        } catch (Exception ex) {
            ex.getMessage();
        }


    }

}
