import java.io.FileNotFoundException;

public class MainClass {
    public static void main(String[] args) throws FileNotFoundException {
        ActivitiesList activities = new ActivitiesList();
        activities.readFromFile(); //1

        Logic functions = new Logic();
        System.out.println("2. Number of distinct days: " + functions.getDayNumber(activities.getDataList())); //2

        functions.getActivityCount(activities.getDataList()); //3
        functions.duration(activities.getDataList()); //5
        functions.entireDuration(activities.getDataList()); //6
        functions.filter(activities.getDataList()); //7
        functions.getActivityPerDay(activities.getDataList()); //4

    }

}
