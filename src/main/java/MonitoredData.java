import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
    private Date startTime; //year-month-date-hrs-min-sec
    private Date endTime; //year-month-date-hrs-min-sec
    private String activityName;

    MonitoredData(Date start, Date end, String name) {
        this.startTime = start;
        this.endTime = end;
        this.activityName = name;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityName() {
        return activityName;
    }

    @Override
    public String toString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return  dateFormat.format(startTime) + "\t\t" +
                dateFormat.format(endTime) + "\t\t" +
                activityName + "\n";
    }

}
