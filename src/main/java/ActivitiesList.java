import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ActivitiesList {
    private final List<MonitoredData> dataList;

    ActivitiesList() {
        dataList = new ArrayList<>();
    }

    public List<MonitoredData> getDataList() {
        return this.dataList;
    }

    //task 1
    void readFromFile() {
        String fileName = "Activities.txt";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Files.lines(Paths.get(fileName))
                    .map(l -> l.split("\\s+"))
                    .forEach(a -> {
                        try {
                            dataList.add(new MonitoredData(format.parse(a[0] + " " + a[1]), format.parse(a[2] + " " + a[3]), a[4]));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.getMessage();
            System.out.println("Wrong file");
        }
    }

}
